void readFiles() {
  //Loads buildings config
  buildings_config = loadJSONArray("buildings.json");
  int buildingsCount = buildings_config.size();
  builds = new Building[buildingsCount];

  //String[] buildings = loadStrings("buildings.txt");
  pushStyle();
  for (int i = 0; i < buildings_config.size(); i++) {
    JSONObject building = buildings_config.getJSONObject(i);

    // Getting object values
    String name = building.getString("name");
    String description = building.getString("description");
    int cost = building.getInt("cost");
    float gain = building.getFloat("gain");

    // Making our buildings object array
    builds[i] = new Building((APP_WIDTH/3*2) + 10, 10 + i*100, name, description, cost, gain, 0, "images/buildings/" + name + ".png", #B2B2B2);
  }
  popStyle();




  //Loads upgrades config file
  upgrades_config = loadJSONArray("upgrades.json");
  int upgradesCount = upgrades_config.size();
  ups = new Upgrade[upgradesCount];
  pushStyle();

  for (int i = 0; i < upgrades_config.size(); i++) {
    JSONObject upgrade = upgrades_config.getJSONObject(i);

    String upgrade_name = upgrade.getString("name");
    String upgrade_description = upgrade.getString("description");
    String affected_building = upgrade.getString("affects");
    int upgrade_multiplier = upgrade.getInt("multiplier");
    int upgrade_cost = upgrade.getInt("cost");
    int upgrade_type = upgrade.getInt("type");

    int cols = 3, rows = 6;
    for (float x = 0; x < width; x+=width/cols) {
      for (float y = 0; y < height; y+=height/rows) {
        rect(x, y, 5, 5);
      }
    }

    for (int j = 0; j < 3; j++) {
      ups[i] = new Upgrade(APP_WIDTH/3 + 15 + j * (i*40), 10, upgrade_name, upgrade_description, upgrade_cost, "images/upgrades/" + upgrade_name + ".png", #B2B2B2, affected_building, upgrade_type, upgrade_multiplier);
    }
  }
  popStyle();

  int cols = 5;
  int rows = ceil(upgrades_config.size() / 5);
  println(cols, rows, upgrades_config.size()/5);
  // Begin loop for columns
  for (int i = 0; i < cols; i++) {
    // Begin loop for rows
    for (int j = 0; j < rows; j++) {

      // Scaling up to draw a rectangle at (x,y)
      int x = i*5;
      int y = j*5;
      fill(255);
      stroke(0);
      // For every column and row, a rectangle is drawn at an (x,y) location scaled and sized by videoScale.
      rect(x, y, 50, 50);
    }
  }



  // Checking if save file exists
  if (dataFile("save.txt").isFile()) {
    println("Save file found. Reading data...");
    save_file = loadStrings("save.txt");
    owned_burgers = float(save_file[0]);
    bps = float(save_file[1]);
    click_multiplier = float(save_file[2]);

    try {
      // Buildings save
      for (int i = 0; i < builds.length; i++) {
        String[] building_data = save_file[i+3].split(" ");
        builds[i].setCount(int(building_data[0]));
        builds[i].setCost(int(building_data[1]));
      }
      
      // Upgrades save
      for (int i = 0; i < ups.length; i++) {
        String[] upgrade_data = save_file[i+3+builds.length].split(" ");
        ups[i].setBought(boolean(upgrade_data[0]));
      }
    }
    catch (ArrayIndexOutOfBoundsException e) {
      println("The save file isn't complete. The missing data will be added on the next game save");
    }
  } else {
    println("No save file found. It will be created when closing the game...") ;
  }
}

void saveGame() {
  String[] scores = {str(owned_burgers), str(bps), str(click_multiplier)};

  for (int i = 0; i < builds.length; i++) {
    scores = (String[]) append(scores, builds[i].getCount()+ " " + builds[i].getCost() + " " + builds[i].getTitle());
  }
  for (int i = 0; i < ups.length; i++) {
    scores = (String[]) append(scores, ups[i].wasBought() + " " + ups[i].getTitle());
  }

  // Writes the strings to a file, each on a separate line
  saveStrings(dataPath("save.txt"), scores);
  println("Closing program...");
  println("Successfully saved game !");
}
