class Building {
  String title, image_path, description;
  float gain;
  int count, posX, posY, H = 90, W = APP_WIDTH - (APP_WIDTH - 410), cost;
  boolean isInside;
  PImage logo;
  color buyableColor;

  Building(int x, int y, String t, String desc , int c, float g, int co, String path, color col) {
    title = t;
    description = desc;
    cost = c;
    gain = g;
    count = co;
    posX = x;
    posY = y;
    image_path = path;
    buyableColor = col;
  }

  public int getCost() {
    return cost;
  }

  public float getGain() {
    return gain;
  }

  public int getCount() {
    return count;
  }

  public String getTitle() {
    return title;
  }

  public String getDescription(){
    return description;
  }

  public int getPosX(){
   return posX;
  }

  public int getPosY(){
   return posY;
  }

  public void setCost(int newCost) {
    cost = newCost;
  }

  public void setGain(float newGain) {
    gain = newGain;
  }

  public void setCount(int newCount) {
    count = newCount;
  }

  public void addCount() {
    count++;
  }

  void scrollUp(){
   posY+=12;
  }

  void scrollDown(){
    posY-=12;
  }

  public void setColor(color newColor) {
    buyableColor = newColor;
  }


  void display() {
    pushStyle();
    noStroke();
    fill(255);
    rect(posX, posY, W, H);
    noStroke();
    fill(buyableColor);
    triangle(posX, posY, posX + 30, posY, posX, posY + 30);
    fill(0);
    textSize(20);
    text(title, posX + 10, posY + H/2);

    // Cost text
    image(burger, posX + 10, posY + (H - 30), 25, 25);
    textSize(12);
    text(nfc(cost), posX + 40, posY + (H - 10));


    //Count text
    textAlign(RIGHT, CENTER);
    textSize(25);
    text(nfc(count), posX + (W-100), posY + H/2);

    logo = loadImage(image_path);
    imageMode(CENTER);
    image(logo, posX + W - 40, posY + H/2, H-20, H-20);
    popStyle();

    over();
    isBuyable();
  }

  boolean isOnMe() { //VERIFICATION DU SURVOL DU BOUTTON RECTANGLE
    return (mouseX>=posX && mouseX<=posX+W) && (mouseY>=posY && mouseY<=posY+H);
  }

  void isBuyable(){
   if(owned_burgers >= cost){
     setColor(#45BF7F);
   }else{
     setColor(#B2B2B2);
   }
  }

  void click() {
    if (owned_burgers >= cost) {
      addCount();
      owned_burgers = owned_burgers - cost;
      float nextCost = cost * 1.15 * int(currentBuyMode);
      setCost(int(nextCost));
      bps = bps + gain;
      saveGame();
    }
  }
}
