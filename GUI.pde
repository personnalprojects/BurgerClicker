void mainCountersGUI(){
  
  // APP TITLE
  pushStyle();
  textFont(font);
  textSize(25);
  textAlign(CENTER, CENTER);
  fill(255);
  text("Burger Clicker", 200, 25);
  popStyle();
  
  // Creating all areas
  pushStyle();
  stroke(255);
  //line(0, 50, 1280, 50);
  line(APP_WIDTH/3, 0, APP_WIDTH/3, APP_HEIGHT);
  line(APP_WIDTH/3*2, 0, APP_WIDTH/3*2, APP_HEIGHT);
  popStyle();
  
  
  // Scketch memory usage display
  long allocatedMemory = Runtime.getRuntime().maxMemory();
  long freeMemory = Runtime.getRuntime().freeMemory();
  long usedMem = allocatedMemory - freeMemory;
  int dataSize = 1024 * 1024;
  pushStyle();
  textSize(10);
  fill(255);
  text("Used mem / Max mem", 10, APP_HEIGHT - 30);
  text(usedMem / dataSize + " / " + allocatedMemory / dataSize + " MB", 10, APP_HEIGHT - 10);
  popStyle();
  
  
  //Placing burger clicker
  pushStyle();
  imageMode(CENTER);
  image(burger, 200, 250, 300, 300);
  
  // Scores display
  textSize(20);
  textAlign(CORNER);
  fill(#9146FF);
  text("Scores :", 22, APP_HEIGHT/3*2+2);
  fill(255);
  text("Scores :", 20, APP_HEIGHT/3*2);
  
  textSize(18);
  fill(255);
  text("Burgers owned : " + nfc(owned_burgers, 2), 20, APP_HEIGHT/3*2 + 40);
  textSize(15);
  text("BPS: " + nfc(bps, 2), 20, APP_HEIGHT/3*2 + 60);
  textSize(15);
  text("Clics : " + nfc(click_multiplier, 2), 20, APP_HEIGHT/3*2 + 80);
  popStyle();
  
  
  // Buy mode display
  //pushStyle();
  //fill(255);
  //rect(APP_WIDTH/3 + 20, APP_HEIGHT - 70, 135, 20);
  //fill(0);
  //textSize(12);
  //text("Change buy mode", APP_WIDTH/3 + 25, APP_HEIGHT - 53);
  //fill(255);
  //text("Buying x" + currentBuyMode, APP_WIDTH/3 + 20, APP_HEIGHT - 30);
  //popStyle();
}
