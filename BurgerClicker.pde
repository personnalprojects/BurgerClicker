// May import minim later;

// App vars
String APP_TITLE = "Burger Clicker - 0 Burgers";
int APP_WIDTH = 1280;
int APP_HEIGHT = 720;

// Tooltips vars
int tooltip_time = millis();

// Font and burger image
PFont font;
PImage burger;

// save file and buildings systems vars
String[] save_file;
JSONArray buildings_config;
Building[] builds;
JSONArray upgrades_config;
Upgrade[] ups;

// Buying system vars
String[] buyModes = {"1", "10", "100"};
String currentBuyMode;

//UPgrades vars
float click_multiplier = 1.00;

// Burgers earning system vars
int time = millis();
float owned_burgers = 0;
float bps = 0;

// Golden burgers system vars
int golden_t_min = 300000;
int golden_t_max = 900000;
int golden_t = golden_t_max - golden_t_min;
ArrayList<GoldenBurger> golden_burgers = new ArrayList<GoldenBurger>();
PImage golden_burger_image;


void setup() {
  readFiles();

  burger = loadImage("images/burger.png");
  golden_burger_image = loadImage("images/miscellaneous/Golden burger.png");
  font = createFont("data/fonts/Gameplay.ttf", 32);

  size(1280, 720);
  background(37, 37, 37);
  surface.setTitle(APP_TITLE);
  surface.setResizable(false);
  surface.setIcon(burger);

  golden_burgers.add(new GoldenBurger(100, 100));
  golden_burgers.remove(0);

  currentBuyMode = buyModes[0];
}

void draw() {
  background(37, 37, 37);
  mainCountersGUI();

  for (Building b : builds) {
    b.display();
  }
  
  for (Upgrade u : ups) {
    u.display();
  }

  // Auto increment burgers for bps
  if (millis() > time + 1000) { // If bps > 0 increment every seconds to owned_burger total
    owned_burgers += bps;
    surface.setTitle("Burger Clicker - " + nfc(owned_burgers, 2) + " Burgers | BPS : " + nfc(bps, 2));
    time = millis();
  }
}


void exit() {
  saveGame();
  super.exit();
}
