class Upgrade {
  String name, description, affected_building;
  int cost, multiplier, type;
  PImage image;
  color fillColor;
  boolean bought = false;

  int W = 70, H = 70, posX, posY;

  Upgrade(int uposX, int uposY, String uname, String udesc, int ucost, String uimage, color ucolor, String uaffected, int utype, int umultiplier) {
    name = uname;
    description = udesc;
    cost = ucost;
    posX = uposX;
    posY = uposY;
    image = loadImage(uimage);
    fillColor = ucolor;
    affected_building = uaffected;
    type = utype;
    multiplier = umultiplier;
  }

  boolean wasBought() {
    return bought;
  }

  String getTitle() {
    return name;
  }
  
  void setBought(Boolean newStatus){
   bought = newStatus; 
  }

  boolean isOnMe() {
    return (mouseX>=posX && mouseX<=posX+W) && (mouseY>=posY && mouseY<=posY+H);
  }

  void isBuyable() {
    if (owned_burgers >= cost && bought == false) {
      setColor(#45BF7F);
    } else {
      setColor(#000000);
    }
  }

  void setColor(color newColor) {
    fillColor = newColor;
  }

  void display() {
    if (bought) {
      pushStyle();
      noStroke();
      tint(92);
      fill(#CCCCCC);
      rect(posX, posY, W+3, H+3);
      fill(255);
      rectMode(CENTER);
      rect(posX + W/2, posY + H/2, W, H);
      imageMode(CENTER);
      image(image, posX + W/2, posY + H/2, W - 10, H - 10);
      popStyle();
    } else {
      isBuyable();
      pushStyle();
      noStroke();
      fill(fillColor);
      rect(posX, posY, W+3, H+3);
      fill(255);
      rectMode(CENTER);
      rect(posX + W/2, posY + H/2, W, H);
      imageMode(CENTER);
      image(image, posX + W/2, posY + H/2, W - 10, H - 10);
      popStyle();
    }
  }

  void click() {
    if (owned_burgers >= cost && bought == false) {
      owned_burgers = owned_burgers - cost;
      bought = true;

      switch(affected_building) {
      case "Click":
        if (type == 1) {
          click_multiplier = click_multiplier * multiplier;
        } else if (type == 2) {
          float percentage = bps * multiplier / 100;
          bps = bps + percentage;
        }
        break;
      }
      saveGame();
    }
  }
}
