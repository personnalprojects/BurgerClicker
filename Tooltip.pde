void showTooltip(String title, String description, int cost, float gain, int count){
  pushStyle();
  rect(mouseX, mouseY, 300, 165);
  textSize(15);
  fill(0);
  text(title, mouseX + 10, mouseY + 25);
  textSize(12);
  text(description, mouseX + 10, mouseY + 40, 280, 600);
  text("Cout : " + nfc(cost, 2) + "   |   Gain : " + nfc(gain, 2), mouseX + 10, mouseY + 140);
  textSize(12);
  text("Gain cumule : " + nfc(gain * count, 2), mouseX + 10, mouseY + 160);
  popStyle();
}
