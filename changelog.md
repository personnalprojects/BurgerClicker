# Burger clicker - CHANGELOG



#### Alpha 0.4.1 - Upgrades System (current)

- Removed buying system

- Added exported apps

- Random fixes

  

#### Alpha 0.4.0 - Upgrades System

- Added upgrades system
- New upgrades :
  - Clic renforcé I & II
  - Burgers boostés I & II
- Added project documentation
- Now saving the game on each user interaction (except  when clicking main burger)
- New icon for Restaurant building
- Random fixes





#### Alpha 0.3.1

 - Added a colored icon when it is possible to buy a building
- Some optimizations
- Added new temporary dedicated icon for Restaurant building



#### Aplha 0.3 - Game features

	- Started preparation for golden burgers implementation
		- Added golden burger image
- Started preparation for buying multipliers system implementation
- Added tooltips when hovering buildings
  - Added buildings description
- Will start to add upgrades system
- Added possibility to scroll through buildings list



#### Aplha 0.2.1 - balancing

	- Balanced prices and gains from buildings :
		- Cursor:
			- Initial Cost : 15 -> 15
			- Gain : 0.1 -> 0.1
		- Mommy :
			- Initial Cost : 75 -> 100
			- Gain : 0.5 -> 1
		- Burger truck :
			- Initial Cost : 150 -> 1100
			- Gain : 1.2 -> 8
		- Fast food :
			- Initial Cost : 300 -> 12000
			- Gain : 2 -> 47
		- Restaurant :
			- Initial Cost : 450 -> 130000
			- Gain : 6 -> 260
- Added minim sound library
- Added number format



#### Alpha 0.2 - SAVE ERA

 - Added save file
- Added memory usage indicator
- Added changelog
- Added new images for buildings (Fast food & Burger truck)
- Added new building :
  - Restaurant (Initial Cost : 450 | Initial Gain : 6)



#### Alpha 0.1 - BASICS

 - Added Clicks
- Added possibility to buy buildings
- Added Buildings list :
  - Cursor (Initial cost : 15 | Initial Gain : 0.1)
  - Mommy (Initial cost : 75 | Initial Gain : 0.5)
  - Burger truck (Initial cost : 150 | Initial Gain : 1.2)
  - Fast food (Initial cost : 300 | Initial Gain : 2)
- New font : Gameplay
