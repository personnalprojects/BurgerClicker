# Burger Clicker - Documentation

Burger Clicker is an Idle Game. The goal is to have the greatest possible number of burgers.

You can buy buildings and upgrades to go faster.



Here's a little document that indicates all the data used by the game



#### Buildings

| Name         | Initial Cost | Initial Gain |
| ------------ | ------------ | ------------ |
| Cursor       | 15           | 0.1          |
| Mommy        | 100          | 1            |
| Burger truck | 1,100        | 8            |
| Fast Food    | 12,000       | 47           |
| Restaurant   | 130,000      | 260          |

Each time you buy a building it's cost is increased by :

```js
newCost = oldCost * 1.15
```

Rounded at the nearest lower digit.



#### Upgrades

| Name             | Cost  | Affected buildings | Type | Effect             |
| ---------------- | ----- | ------------------ | ---- | ------------------ |
| Clic renforcé    | 100   | Clics              | 1    | x2 on clics        |
| Clic renforcé II | 500   | Clics              | 1    | x2 on clics        |
| Burgers bosotés  | 10000 | Clics              | 2    | add x0.1bps to bps |

Legend :

- Clics :
  - Type 1 : Clic souris
  - Type 2 : BPS



**Upgrades can only be bought once.**



#### Save file - Structure

- Total owned burgers
- Burgers per seconds
- For Buildings
  - Building count - Building Cost - Building Name
- For upgrades
  - Is upgrade bought - Upgrade name

```bash
# Exemple save file (not including all buildings and upgrades)

2039881.5
7091.603
66 122523 Cursor
45 52330 Mommy
33 110538 Burger truck
28 600689 Fast food
21 2446755 Restaurant
true Clic renforcé
false Clic renforcé II
```

