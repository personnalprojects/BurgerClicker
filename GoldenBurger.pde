class GoldenBurger{
  int W = 30, H = 30;
  int posX, posY;
 
  GoldenBurger(int px, int py){
    posX = px;
    posY = py;
  }
  
  void display(){
    image(golden_burger_image, posX, posY, W, H);
  }
  
}
