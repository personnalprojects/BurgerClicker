void mousePressed() {
  // Click on building
  boolean buttonClicked=false;//Le bouton a-t-il été cliqué?
  for (int i=0; i<builds.length; i++) {//On regarde si on clique sur un bouton
    if (builds[i].isOnMe()) {
      buttonClicked=true;
      builds[i].click();
      break;
    }
  }

  // Click on upgrade
  boolean upgradeClicked=false;//L'upgrade a-t-elle été cliquée?
  for (int i=0; i<ups.length; i++) {//On regarde si on clique sur une upgrade
    if (ups[i].isOnMe()) {
      upgradeClicked=true;
      ups[i].click();
      break;
    }
  }

  // Click on burger
  if ((mouseX>= 50 && mouseX<= 350) && (mouseY>= 100 && mouseY<= 400)) {
    owned_burgers = owned_burgers + 1 * click_multiplier;
    surface.setTitle("Burger Clicker - " + nfc(owned_burgers, 2) + " Burgers | BPS : " + nfc(bps, 2));
  }

  //// Change buy mode
  //if ((mouseX>= APP_WIDTH/3*2 + 20 && mouseX<= (APP_WIDTH/3*2) + 20 + 135) && (mouseY>= APP_HEIGHT - 70 && mouseY<= (APP_HEIGHT - 70) + 15)) {
  //  if (currentBuyMode == buyModes[0]) {
  //    currentBuyMode = buyModes[1];
  //    //updatePrices();
  //  } else if (currentBuyMode == buyModes[1]) {
  //    currentBuyMode = buyModes[2];
  //    //updatePrices();
  //  } else {
  //    currentBuyMode = buyModes[0];
  //    //updatePrices();
  //  }
  //}
}


void mouseWheel(MouseEvent event) {
  // Scoll for building list
  if (mouseX > APP_WIDTH/3*2) {
    for (int i=0; i<builds.length; i++) {//On regarde si on clique sur un bouton
      switch(event.getCount()) {
      case -1:
        builds[i].scrollUp();
        break;
      case 1:
        builds[i].scrollDown();
        break;
      }
    }
  }
}

int wait = 1500;
int time_before_tooltip = millis();

void over() {
  for (int i = 0; i < builds.length; i++) {
    if (builds[i].isOnMe()) {
      //println("Wait:"+wait);
      //println("timebefore:" + time_before_tooltip);
      showTooltip(builds[i].title, builds[i].description, builds[i].cost, builds[i].gain, builds[i].count);
    }
  }
}
